import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./styles/variables.scss";
import { useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import Header from "./components/Header";
import Home from "./pages/Home/Home";
import Shop from "./pages/Shop";
import Blog from "./pages/Blog/Blog";
import AboutUs from "./pages/About Us/AboutUs";
import ContactUs from "./pages/Contact Us/ContactUs";
import Footer from "./components/Footer";
import ScrollToTop from "./components/ScrollToTop";
import WebsiteLoader from "./components/WesiteLoader";

function App() {
  useEffect(() => {
    return () => {
      window.removeEventListener("scroll", () => handleScroll);
    };
  }, []);

  const [isSticky, setSticky] = useState(false);

  const handleScroll = () => {
    window.pageYOffset > 70 ? setSticky(true) : setSticky(false);
  };

  window.addEventListener("scroll", handleScroll);

  return (
    <div>
      <React.Suspense fallback={<WebsiteLoader />}>
        <ScrollToTop />
        <Header sticky={isSticky} />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/shop" element={<Shop />} />
          <Route path="/blog" element={<Blog />} />
          <Route path="/about-us" element={<AboutUs />} />
          <Route path="/contact-us" element={<ContactUs />} />
        </Routes>
        <Footer />
      </React.Suspense>
    </div>
  );
}

export default App;
