import "../../styles/Home.scss";
import "../../styles/About.scss";
import "../../styles/Blog.scss";
import HeaderSection from "../About Us/HeaderSection";
import AllBlogs from "./AllBlogs";

const Blog = () => {
  return (
    <div>
      <HeaderSection title="CureEasy Blogs" />
      <AllBlogs />
    </div>
  );
};

export default Blog;
