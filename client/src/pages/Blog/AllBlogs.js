import BlogCard from "../Home/BlogCard";
import { blogInfoData } from "../Home/blogData";

const AllBlogs = () => {
  return (
    <div className="container-fluid">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="blog-grid ">
                {
                    blogInfoData.map((data, index) => (
                        <BlogCard key={index} className="item" data={data} />
                    ))
                }
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AllBlogs;