const InfoSection = () => {
  return (
    <div className="container-fluid mb-5">
      <div className="container">
        <div className="row">
          <div className="col-lg-5 col-md-4 col-sm-12 col-xs-12">
            <h6 className="text-uppercase">About Us</h6>
            <h1 className="sechead">
              The Best Medicines, Doctors & Physicians
            </h1>
          </div>
          <div className="col-lg-7 col-md-8 col-sm-12 col-xs-12">
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus
              distinctio optio adipisci similique, nemo doloremque amet mollitia
              commodi. Laudantium commodi iusto itaque quidem quibusdam
              consectetur. Animi perferendis numquam odio modi. Lorem ipsum
              dolor sit, amet consectetur adipisicing elit. Laboriosam fugiat
              possimus ullam deserunt, suscipit cum beatae placeat fugit
              eligendi ab ipsam! Neque sit accusamus pariatur cumque eum nisi
              praesentium veniam.
            </p>

            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum
              reprehenderit numquam culpa. Nostrum repellat ipsa, nobis
              voluptate adipisci temporibus reiciendis optio amet quaerat
              nesciunt, dolores at similique a? Ut, a! Lorem ipsum dolor sit
              amet, consectetur adipisicing elit. Aliquam esse aliquid
              voluptatem minus velit quaerat nemo explicabo magni labore fugit,
              error laborum voluptates quae! Itaque consequatur quisquam aliquid
              perferendis unde.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InfoSection;
