import specialistimg from "../../Images/specialistimg.jpg";

const AdvantagesSection = () => {
  return (
    <div className="container-fluid advantages-section py-5 mb-5">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 py-4">
            <h6 className="text-uppercase">What We Offer</h6>
            <h1 className="sechead">Our Advantages</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet, aspernatur? Eligendi rem vero quo accusamus ipsam a doloremque illum accusantium iure voluptate. Dolore consectetur iusto natus, impedit est nemo deserunt!</p>
            <ul className="advantages-list">
                <li>Specialists and Dentists as One Team</li>
                <li>Complimentary Consultations</li>
                <li>All Types of Dental Services</li>
                <li>On-site Laborator</li>
            </ul>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <img src={specialistimg} alt="advantages-img" className="advantages-img" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdvantagesSection;
