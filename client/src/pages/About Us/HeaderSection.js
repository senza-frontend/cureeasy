const HeaderSection = ({ title }) => {
  return (
    <div className="container-fluid about-header">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="header-text text-center">
              <h1 className="text-light headingh1 sechead">{title}</h1>
              <p className="text-light">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod
                laudantium, quae ratione magni consectetur natus est labore
                cumque! Aspernatur optio porro nostrum distinctio eum eaque quod
                perferendis consequatur nisi obcaecati.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeaderSection;
