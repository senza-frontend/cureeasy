import { Link } from "react-router-dom";
import { RiFacebookFill, RiTwitterFill, RiInstagramFill } from "react-icons/ri";

const MemberCard = ({ photo, name, role, fb, tw, ig }) => {
  return (
    <div className="member-card">
      <img src={photo} alt="member-img" className="member-img" />
      <div className="member-info text-center">
        <h4>{name}</h4>
        <h6>{role}</h6>
        <div className="member-sa">
          <Link to={fb} className="fb">
            <RiFacebookFill />
          </Link>
          <Link to={tw} className="tw">
            <RiTwitterFill />
          </Link>
          <Link to={ig} className="ig">
            <RiInstagramFill />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default MemberCard;
