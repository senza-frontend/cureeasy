import { FaRegLightbulb, FaRegHandshake, FaPodcast } from "react-icons/fa";

export const corevaluesData = [
  {
    icon: <FaRegLightbulb />,
    title: "Innovation",
    info: "Lorem ipsum dolor sit amet consectetur, adipisicing elit.",
  },
  {
    icon: <FaRegHandshake />,
    title: "Compassion",
    info: "Alias nisi explicabo obcaecati qui quisquam fugit aspernatur iusto voluptatem.",
  },
  {
    icon: <FaPodcast />,
    title: "Integrity",
    info: "Neque explicabo tempora maxime unde incidunt consectetur asperiores totam.",
  },
];

const CoreValues = () => {
  return (
    <div className="container-fluid corevalues-section mb-5">
      <div className="container">
        <div className="row mb-4">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <h6 className="text-uppercase">Motivation is Key</h6>
            <h1 className="sechead">Our Core Values</h1>
          </div>
        </div>
        <div className="row">
          {corevaluesData.map((cvd, index) => (
            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12" key={index}>
              <div className="cv-card text-center">
                <h1>{cvd.icon}</h1>
                <h3>{cvd.title}</h3>
                <p>{cvd.info}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default CoreValues;
