import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import { ourteamData } from "./ourteamData";
import MemberCard from "./MemberCard";

const OurTeamSection = () => {

    const options = {
    margin: 10,    
    nav: true,
    autoplay: false,
    smartSpeed: 1000,
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      600: {
        items: 1,
      },
      700: {
        items: 1,
      },
      1000: {
        items: 2,
      },
      1200: {
        items: 4,
      },
    },
  };

  return (
    <div className="container-fluid ourteam-section">
      <div className="container">
        <div className="row">
        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <h6 className="text-uppercase">Leading Team</h6>
            <h1 className="sechead text-light">Meet Our Team</h1>
        </div>
        </div>
        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="team-carousel">
              <OwlCarousel className="owl-theme" loop nav {...options}>
                {ourteamData.map((ot, index) => (
                  <MemberCard
                    key={index}
                    photo={ot.photo}
                    name={ot.name}
                    role={ot.role}
                    fb={ot.fbAccoount}
                    tw={ot.twitterAccount}
                    ig={ot.igAccount}
                  />
                ))}
              </OwlCarousel>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};


export default OurTeamSection;