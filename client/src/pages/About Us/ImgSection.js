import AboutUsImg from "../../Images/AboutUsImg.jpg";

const ImgSection = () => {
  return (
    <div className="container-fluid">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <img src={AboutUsImg} alt="aboutusimg" className="aboutusimg" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ImgSection;
