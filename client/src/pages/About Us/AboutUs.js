import "../../styles/Home.scss";
import "../../styles/About.scss";
import HeaderSection from "./HeaderSection";
import ImgSection from "./ImgSection";
import InfoSection from "./InfoSection";
import CoreValues from "./CoreValues";
import AdvantagesSection from "./AdvantagesSection";
import OurTeamSection from "./OurTeamSection";

const AboutUs = () => {
  return (
    <div>
      <HeaderSection title="About CureEasy" />
      <ImgSection />
      <InfoSection />
      <CoreValues />
      <AdvantagesSection />
      <OurTeamSection />
    </div>
  );
};

export default AboutUs;
