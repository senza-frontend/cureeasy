import { useEffect } from "react";
import HeaderSection from "./HeaderSection";
import AboutUsSection from "./AboutUsSection";
import OurServicesSection from "./OurServicesSection";
import AutoCounterSection from "./AutoCounterSection";
import BlogSection from "./BlogSection";
import AppointmentSection from "./AppointmentSection";
import TestinomialsSection from "./TestinomialsSection";
import Aos from "aos";
import "aos/dist/aos.css";

const Home = () => {

  useEffect(() => {
    Aos.init({
      duration: 500,
      easing: 'ease-in-sine',
      delay: 100,
    });
  });

  return (
    <div>
      <HeaderSection />
      <AboutUsSection />
      <OurServicesSection />
      <AutoCounterSection />
      <BlogSection />
      <TestinomialsSection />
      <AppointmentSection />
    </div>
  );
};

export default Home;
