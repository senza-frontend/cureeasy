import "../../styles/Home.scss";
import "../../styles/Button.scss";
import HeaderCard from "../Home/HeaderCard";
import { BsFillPeopleFill, BsFillShieldFill, BsGlobe2 } from "react-icons/bs";
import { MdOutlineDoubleArrow } from "react-icons/md";
import { Link } from "react-router-dom";

const HeaderSection = () => {
  return (
    <div className="container-fluid header-section">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="header-text text-center">
              <h3 className="text-light">Welcome to CureEasy</h3>
              <h1 className="text-light headingh1 sechead">
                We Care and Protect your Health!
              </h1>
              <p className="text-light">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                Exercitationem a nemo voluptatum molestias? Dicta labore ipsum
                quos voluptatem mollitia et, consequuntur explicabo harum animi
                libero! Odio nulla totam fugit neque.
              </p>
              <Link to="/">
                <button className=" ce-btn ce-btn-light">
                  Read More <MdOutlineDoubleArrow />
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>

      <section className="card-section">
        <div className="container">
          <div className="row">
            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <HeaderCard
                cardicon={<BsFillPeopleFill />}
                cardhead={"protect"}
                cardname={"Qualified Team"}
              />
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <HeaderCard
                cardicon={<BsFillShieldFill />}
                cardhead={"help"}
                cardname={"Quality Service"}
              />
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <HeaderCard
                cardicon={<BsGlobe2 />}
                cardhead={"world"}
                cardname={"Global Work"}
              />
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default HeaderSection;
