import { blogInfoData } from "./blogData";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import BlogCard from "./BlogCard";

const BlogSection = () => {
  const options = {
    nav: true,
    autoplay: false,
    smartSpeed: 1000,
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      600: {
        items: 1,
      },
      700: {
        items: 1,
      },
      1000: {
        items: 2,
      },
      1200: {
        items: 3,
      },
    },
  };

  return (
    <div className="container-fluid mb-5 blog-section">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <h6 className="text-uppercase">Latest News</h6>
            <h1 className="sechead">Blog Posts</h1>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="blog-carousel" data-aos="fade-up">
              <OwlCarousel className="owl-theme" loop nav {...options}>
                {blogInfoData.map((data, index) => (
                  <BlogCard key={index} className="item" data={data} />
                ))}
              </OwlCarousel>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BlogSection;
