import { useState } from "react";
import "../../styles/Home.scss";
import { MdOutlineDoubleArrow } from "react-icons/md";
import axios from "axios";
import MailLoader from "./MailLoader";

const AppointmentSection = () => {
  const [name, setName] = useState("");
  const [contact, setContact] = useState("");
  const [email, setEmail] = useState("");
  const [date, setDate] = useState("");
  const [specilization, setSpecilization] =useState("");
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e) => {
    if (name && contact && email && date !== "") {
      e.preventDefault();
      setLoading(true);
      console.log({ name, contact, email, date, specilization });

      const body = {
        name,
        contact,
        email,
        date,
        specilization,
      };

      await axios
        .post("http://localhost:8001/mail", body, {
          headers: {
            "Content-type": "application/json",
          },
        })
        .then((res) => {
          alert("Email Sent Successfully");
          setLoading(false);
          console.log(res);
          window.location.reload();
        })
        .catch((err) => {
          console.log(err);
          setLoading(false);
        });
    } else {
      alert("Please fill all required filled!");
    }
  };

  if (loading) {
    return <MailLoader />
  }
              
  return (
    <div className="container-fuild appointment-section">
      <div className="container">
        <div className="row">
          <div
            className="col-lg-7 col-md-6 col-xs-12 col-sm-6"
            data-aos="fade-right"
          >
            <h6 className="text-uppercase">Free Appointment</h6>
            <h1 className="text-light sechead">Make An Appointment</h1>
            <p className="text-light">
              We believe in providing the best possible care to all our existing
              patients and welcomenew patients to sample.
            </p>
            <p className="text-light">
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolorum,
              tempora. Facere vero sed itaque sunt obcaecati quis officia nam.
            </p>
          </div>
          <div className="col-lg-5 col-md-6 col-xs-12 col-sm-6">
            <div className="appointment-form" data-aos="fade-down">
              <form onSubmit={handleSubmit} method="post">

                

                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <input
                      type="text"
                      placeholder="Full Name"
                      className="form-control mb-4"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                    />
                    <input
                      type="tel"
                      placeholder="Phone"
                      className="form-control mb-4"
                      value={contact}
                      onChange={(e) => setContact(e.target.value)}
                    />
                      <select className="form-control mb-4" onChange={(e) => setSpecilization(e.target.value)} value={specilization}>
                        <option value="Specilization">Specilization</option>
                        <option value="Cardiology">Cardiology</option>
                        <option value="Heart Surgery">Heart Surgery</option>
                        <option value="Skin Care">Skin Care</option>
                        <option value="Body Check-up">Body Check-up</option>
                        <option value="Numerology">Numerology</option>
                        <option value="Diagnosis">Diagnosis</option>
                        <option value="Others">Others</option>
                      </select>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <input
                      className="form-control mb-4"
                      type="email"
                      placeholder="Email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                    <input
                      className="form-control mb-4"
                      type="date"
                      placeholder="Select Date"
                      value={date}
                      onChange={(e) => setDate(e.target.value)}
                    />
                    <button
                      type="submit"
                      className="ce-btn ce-btn-dark float-end"
                      disabled={loading}
                    >
                      <span className="bpbtnspan">Book Appointment</span>
                      <MdOutlineDoubleArrow />
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AppointmentSection;
