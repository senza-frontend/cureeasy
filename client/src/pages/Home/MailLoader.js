import mailloader from "../../Images/mailloader.gif";
import "../../styles/Home.scss";

const MailLoader = () => {
  return (
    <div className="mail-loader-overlay">
      <img src={mailloader} alt="mailloader" />
    </div>
  );
};

export default MailLoader;
