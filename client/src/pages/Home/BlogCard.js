import { IoPersonCircleOutline } from "react-icons/io5";
import { AiOutlineComment, AiTwotoneCalendar } from "react-icons/ai";

const BlogCard = ({ data }) => {
  return (
    <div className="blog-card">
      <img src={data.blogImg} alt="blog-img" className="mb-3" />
      <div>
        <h6 className="text-uppercase">{data.category}</h6>
        <h4>{data.title}</h4>
        <p>
          {data.description.slice(0, 50)} ...<a target="_blank" className="blog-url" href={data.url}>read more</a>
        </p>
        <div className="d-flex justify-content-between align-items-center">
          <p>
            <IoPersonCircleOutline className="blog-icon" /> by {data.author}
          </p>
          <p>
            <AiTwotoneCalendar className="blog-icon" />  {data.date}
          </p>
          <p>
            <AiOutlineComment className="blog-icon" /> {data.comments}
          </p>
        </div>
      </div>
    </div>
  );
};

export default BlogCard;
