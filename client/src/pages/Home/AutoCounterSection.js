import { FaUserMd, FaCalendarPlus, FaSmile, FaTrophy } from "react-icons/fa";
import { CountUp } from "use-count-up";

const AutoCounterSection = () => {
  return (
    <div className="container-fluid couter-section mb-5">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="couter-box d-flex justify-content-between">
              <div className="single-counter text-center" data-aos="zoom-in-up" data-aos-delay="100">
                <FaUserMd size={50} className="p-2 cs-icon" />
                <h1><CountUp isCounting end={25} duration={5} /></h1>
                <p className="mb-0 h5">Expert Doctors</p>
              </div>
              <div className="single-counter text-center" data-aos="zoom-in-up" data-aos-delay="300">
                <FaCalendarPlus size={50} className="p-2 cs-icon"/>
                <h1><CountUp isCounting end={86} duration={5} /></h1>
                <p className="mb-0 h5">Health Programs</p>
              </div>
              <div className="single-counter text-center" data-aos="zoom-in-up" data-aos-delay="500">
                <FaSmile size={50} className="p-2 cs-icon" />
                <h1><CountUp isCounting end={128} duration={5} /></h1>
                <p className="mb-0 h5">Happy Clients</p>
              </div>
              <div className="single-counter text-center" data-aos="zoom-in-up" data-aos-delay="700">
                <FaTrophy size={50} className="p-2 cs-icon" />
                <h1><CountUp isCounting end={55} duration={5} /></h1>
                <p className="mb-0 h5">Success Meets</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AutoCounterSection;
