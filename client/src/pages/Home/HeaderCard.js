import "../../styles/Home.scss";

const HeaderCard = (props) => {
  console.log(props);

  return (
    <div className="header-card">
      <div className="d-flex">
        <h2 className="me-4 card-icon">{props.cardicon}</h2>
        <div>
          <h6 className="text-uppercase">{props.cardhead}</h6>
          <h3 className="sechead">{props.cardname}</h3>
        </div>
      </div>
      <p>Lorem ipsum dolor sit amet adipisicing consectetur elit.</p>
    </div>
  );
};

export default HeaderCard;
