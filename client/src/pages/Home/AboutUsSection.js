import aboutus from "../../Images/aboutus.jpg";
import { MdOutlineDoubleArrow } from "react-icons/md";
import { Link } from "react-router-dom";
import "../../styles/Home.scss";
import "../../styles/Button.scss";

const AboutUsSection = () => {
  return (
    <div className="container-fluid aboutus-section mb-5">
      <div className="container">
        <div className="row">
          <div className="col-lg-8 col-md-6 col-sm-12 col-xs-12 aboutus-text">
            <div data-aos="fade-right">
              <h6 className="text-uppercase">About Us</h6>
              <h1 className="sechead">
                Changing the way you receive healthcare.
              </h1>
              <p>
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab
                ipsum amet eligendi consequuntur magnam ad ratione. Molestias
                repellat ad laboriosam quos officiis. Atque labore iste
                praesentium. Maxime consequatur a reprehenderit.
              </p>
              <Link to="/about-us">
                <button className="ce-btn ce-btn-dark">
                  Read More <MdOutlineDoubleArrow />
                </button>
              </Link>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-12 col-xs-12 aboutus-image">
            <img data-aos="fade-left" src={aboutus} alt="aboutusimg" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutUsSection;
