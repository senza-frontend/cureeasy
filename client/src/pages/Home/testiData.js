export const testiData = [
  {
    name: "Krishna Shah",
    image: "assets/Images/testi1.jpg",
    city: "Surat",
    reviewText: "Itaque laudantium asperiores, qui blanditiis atque magnam, reiciendis deserunt sint, voluptate repudiandae quia explicabo aliquam ratione suscipit iure debitis ea voluptas sapiente? Officiis facilis molestias blanditiis ipsam voluptatum reiciendis fuga ducimus assumenda dolorem maiores iure numquam excepturi nulla, optio libero eum cupiditate veniam quia.",
  },
  {
    name: "Prem Germanwala",
    image: "assets/Images/testi8.jpg",
    city: "Pune",
    reviewText: "Ipsam dolorum, asperiores sed incidunt expedita a minima. Non ex animi eaque fugiat delectus doloremque tempore quae similique reiciendis vel, quaerat iste.Nulla, delectus! Quidem amet minus similique optio distinctio porro vel voluptatibus perspiciatis culpa labore veniam at, facere assumenda reprehenderit deleniti dolor autem!",
  },
  {
    name: "Milin Vaniyawala",
    image: "assets/Images/testi9.jpg",
    city: "Ahmedabad",
    reviewText: "Officiis facilis molestias blanditiis ipsam voluptatum reiciendis fuga ducimus assumenda dolorem maiores iure numquam excepturi nulla, optio libero eum cupiditate veniam quia.Ipsam dolorum, asperiores sed incidunt expedita a minima. Non ex animi eaque fugiat delectus doloremque tempore quae similique reiciendis vel, quaerat iste.",
  },
  {
    name: "Mansi Khant",
    image: "assets/Images/testi3.jpg",
    city: "Rajkot",
    reviewText: "Nulla, delectus! Quidem amet minus similique optio distinctio porro vel voluptatibus perspiciatis culpa labore veniam at, facere assumenda reprehenderit deleniti dolor autem!Repellendus iure, eum fugiat quod illo illum alias, culpa, minus repellat delectus distinctio! Hic eaque quos quas aliquid, vero officia qui cum.",
  },
  {
    name: "Vaishnavi Sarang",
    image: "assets/Images/testi4.jpg",
    city: "Surat",
    reviewText: "Repellendus iure, eum fugiat quod illo illum alias, culpa, minus repellat delectus distinctio! Hic eaque quos quas aliquid, vero officia qui cum.Amet fugit quidem, ducimus quis temporibus, repellat architecto veritatis hic sunt pariatur animi consectetur dolorem natus magnam vero, iste dicta libero veniam!",
  },
  {
    name: "Yash Patel",
    image: "assets/Images/testi6.jpg",
    city: "Mumbai",
    reviewText: "Amet fugit quidem, ducimus quis temporibus, repellat architecto veritatis hic sunt pariatur animi consectetur dolorem natus magnam vero, iste dicta libero veniam! Itaque laudantium asperiores, qui blanditiis atque magnam, reiciendis deserunt sint, voluptate repudiandae quia explicabo aliquam ratione suscipit iure debitis ea voluptas sapiente? ",
  },
];
