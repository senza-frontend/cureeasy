import ServicesCard from "./ServicesCard";

const OurServicesSection = () => {
  const servicesData = [
    {
      serviceName: "Pharmacology",
      description: "Lorem ipsum dolor sit amet consectetur",
      servIcon: "assets/Images/clinic-medical-solid.svg",
    },
    {
      serviceName: "Therapiya",
      description: "Adipisicing elit",
      servIcon: "assets/Images/american-sign-language-interpreting-solid.svg",
    },
    {
      serviceName: "Cardiology",
      description: "Ncmniokl perferendis commodi",
      servIcon: "assets/Images/heartbeat-solid.svg",
    },
    {
      serviceName: "Dentistry",
      description: "Cupiditate delectus ipsam neque",
      servIcon: "assets/Images/superpowers-brands.svg",
    },
    {
      serviceName: "Virusology",
      description: "Expedita deserunt quod",
      servIcon: "assets/Images/viruses-solid.svg",
    },
    {
      serviceName: "Eye Surgery",
      description: "Consectetur aspernatur itaque porro",
      servIcon: "assets/Images/eye-solid.svg",
    },
  ];

  console.log(servicesData);

  return (
    <div className="container-fluid services-section mb-5">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <h6 className="text-uppercase">Extraordinary services</h6>
            <h1 className="sechead">Our Services</h1>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="services-div">
              {servicesData.map((sd, index) => (
                <ServicesCard
                  sdimg={sd.servIcon}
                  sdname={sd.serviceName}
                  sddes={sd.description}
                  key={index}
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OurServicesSection;
