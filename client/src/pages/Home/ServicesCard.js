const ServicesCard = ({sdimg, sdname, sddes}) => {
    return (
        <div className="services-card d-flex mb-4 align-items-center" data-aos="fade-up" >
            <div className="me-4 services-icon">
                <img src={sdimg} alt="serviceImg" width={40}/>
            </div>
            <div>
                <h3>{sdname}</h3>
                <p>{sddes}</p>
            </div>
        </div>
    );
};

export default ServicesCard;