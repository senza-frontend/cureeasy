import { testiData } from "./testiData";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import "../../styles/Home.scss";
import Particles from "react-tsparticles";
import { particlesConfig } from "./particles-config";

const TestinomialsSection = () => {
  const options = {
    autoplay: true,
    smartSpeed: 1000,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      600: {
        items: 1,
      },
      700: {
        items: 1,
      },
      1000: {
        items: 1,
      },
    },
  };

  return (
    <div className="container-fluid testi-section mb-5">
      <Particles params={particlesConfig}></Particles>
      <div className="container">
        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <h6 className="text-uppercase">Happy Clients</h6>
            <h1 className="sechead">What Our Clients Say</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="testi-carousel" data-aos="flip-up">
              <OwlCarousel className="owl-theme" loop {...options}>
                {testiData.map((td, index) => (
                  <div className="item testi-card" key={index}>
                    <div className="review-text">
                      <p>
                        {td.reviewText}
                        <strong className="h5">"</strong>
                      </p>
                    </div>
                    <div className="review-by-info d-flex">
                      <img src={td.image} alt="reviewimg" className="me-4" />
                      <div>
                        <h5>{td.name}</h5>
                        <p>{td.city}</p>
                      </div>
                    </div>
                  </div>
                ))}
              </OwlCarousel>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TestinomialsSection;
