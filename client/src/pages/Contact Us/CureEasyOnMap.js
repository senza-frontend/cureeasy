const CureEasyOnMap = () => {
  return (
    <div className="container-fluid">
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3719.8098402767805!2d72.79148881533396!3d21.199711787295165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04d43c8f3f8c7%3A0x75338595ce918321!2sInfozium%20Solutions%20Pvt.%20Ltd.!5e0!3m2!1sen!2sin!4v1642584990006!5m2!1sen!2sin"
        height={450}
        loading="lazy"
        title="CureEasyOnMap"
      ></iframe>
    </div>
  );
};

export default CureEasyOnMap;
