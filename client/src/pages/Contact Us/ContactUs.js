import "../../styles/Home.scss";
import "../../styles/About.scss";
import HeaderSection from "../About Us/HeaderSection";
import ContactSection from "./ContactSection";
import CureEasyOnMap from "./CureEasyOnMap";

const ContactUs = () => {
    return (
        <div>
            <HeaderSection title="Contact Us" />
            <ContactSection />
            <CureEasyOnMap />
        </div>
    );
};

export default ContactUs;