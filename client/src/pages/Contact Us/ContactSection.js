import { FaMapMarkerAlt, FaPhoneAlt, FaEnvelope, FaRegLifeRing } from "react-icons/fa";

export const contactInfo = [
  {
    icon: <FaMapMarkerAlt />,
    title: "Location",
    info: "417-426, Sovereign Shoppers, Anand Mahal Rd, beside Sindhu Seva Samiti School, Adajan, Surat, Gujarat 395009",
  },
  {
    icon: <FaPhoneAlt />,
    title: "Call Us",
    info: "+(21) 255 999 8888",
  },
  {
    icon: <FaEnvelope />,
    title: "Email Us",
    info: "inquiry@cureeasy.com",
  },
  {
    icon: <FaRegLifeRing />,
    title: "Customer Support",
    info: "support@cureeasy.com",
  },
];

const ContactSection = () => {
  return (
    <div className="container-fluid my-5">
      <div className="container py-5">
        <div className="row">
          <div className="col-lg-7 col-md-6 col-sm-12 col-xs-12">
            <h6 className="text-uppercase">Get In Touch</h6>
            <h1 className="sechead">Contact Us</h1>
            <p>
              We have a dedicated support center for all of your support needs.
              We usually get back to you within 12-24 hours.
            </p>
            {
                contactInfo.map((c, index) => (
                    <div key={index} className="d-flex mb-3">
                        <h4 className="me-3 ce-icon">{c.icon}</h4>
                        <div>
                            <h4>{c.title}</h4>
                            <h5>{c.info}</h5>
                        </div>
                    </div>
                ))
            }
          </div>
          <div className="col-lg-5 col-md-6 col-sm-12 col-xs-12">
            <div className="contact-form m-auto py-5">
                <form>
                    <input type="text" name="name" placeholder="Name" className="form-control mb-4" />
                    <input type="email" name="email" placeholder="Email" className="form-control mb-4" />
                    <input type="tel" name="mobile" placeholder="Phone" className="form-control mb-4" />
                    <input type="text" name="subject" placeholder="Subject" className="form-control mb-4" />
                    <textarea name="message" placeholder="Your Message" className="form-control mb-4"></textarea>
                    <button className="ce-btn ce-btn-dark float-end">Send Message</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactSection;
