import { useState } from "react";
import { Link } from "react-router-dom";
import "../styles/Navbar.scss";
import cureeasymain from "../Images/CureEasytransparent.png";
// import { BsList, BsXCircle } from "react-icons/bs";

const Header = ({ sticky }) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <header className={sticky ? "sticky" : " "}>
      <nav className="navbar navbar-light navbar-expand-lg">
        <div className="container-fluid">
          <div className="container">
            <div className="row">
              <div className="col-lg-2 col-md-2 col-sm-8 col-xs-col-8 col-6">
                <Link to="/">
                  <img src={cureeasymain} alt="logo" width={200} />
                </Link>
              </div>
              <div className="col-lg-10 col-md-10 col-sm-4 col-xs-col-4 col-6">
                <div className="float-end">
                  <button
                    className={`mb-menu ${isOpen ? "mopen" : "close"}`}
                    onClick={() => setIsOpen(!isOpen)}
                  ></button>
                  <ul className={`navbar-nav ${isOpen ? "mopen" : "close"}`}>
                    <li className="nav-item">
                      <Link className="nav-link mx-lg-2 nav-link-color" to="/">
                        Home
                      </Link>
                    </li>
                    {/* <li className="nav-item">
                      <Link
                        className="nav-link mx-lg-2 nav-link-color"
                        to="/shop"
                      >
                        Shop
                      </Link>
                    </li> */}
                    <li className="nav-item">
                      <Link
                        className="nav-link mx-lg-2 nav-link-color"
                        to="/blog"
                      >
                        Blog
                      </Link>
                    </li>
                    <li className="nav-item">
                      <Link
                        className="nav-link mx-lg-2 nav-link-color"
                        to="/about-us"
                      >
                        About
                      </Link>
                    </li>
                    <li className="nav-item">
                      <Link
                        className="nav-link mx-lg-2 nav-link-color"
                        to="/contact-us"
                      >
                        Contact
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Header;
