const WebsiteLoader = () => {
    return (
        <div className="container-fuild h-100 position-relative">
            <div className="loader">
                <img src="/assets/Images/CEloader.gif" />
            </div>
        </div>
    );
};

export default WebsiteLoader;