import "../styles/Footer.scss";
import cureeasymain from "../Images/CureEasytransparent.png";
import {
  RiMailLine,
  RiPhoneLine,
  RiArrowRightCircleLine,
  RiFacebookFill,
  RiTwitterFill,
  RiInstagramLine,
  RiLinkedinFill,
} from "react-icons/ri";
// import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <footer>
      <div className="container-fluid">
        <div className="container">
          <div className="row">
            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-5">
              <img src={cureeasymain} alt="logo" className="footer-logo" />
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor
                sit amet, consectetur!
              </p>
              <p>
                <RiPhoneLine /> +(21) 255 999 8888
              </p>
              <p>
                <RiMailLine /> cureeasy@mail.com
              </p>
            </div>
            <div className="col-lg-2 col-md-2 col-6 mb-5">
              <h3 className="text-light">History</h3>
              <ul className="ps-0">
                <li>About Us</li>
                <li>Blog Posts</li>
                <li>Departments</li>
                <li>Contact us</li>
              </ul>
            </div>
            <div className="col-lg-2 col-md-2 col-6 mb-5">
              <h3 className="text-light">Useful Links</h3>
              <ul className="ps-0">
                <li>Terms of service</li>
                <li>Privacy policy</li>
                <li>Documentation</li>
                <li>Support</li>
              </ul>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-5">
              <h3 className="text-light">Subscribe to our Newsletter</h3>
              <p>
                Enter your email and receive the latest news, updates and
                special offers from us.
              </p>
              <form action="">
                <input
                  type="email"
                  className="form-control"
                  placeholder="Email"
                />
                <button className="subscribe-btn">
                  <RiArrowRightCircleLine />
                </button>
              </form>
            </div>
          </div>
        </div>
        <hr />
        <div className="container py-3">
          <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <h5>
                © 2021 <span>CureEasy</span>. All rights reserved.
              </h5>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div className="sm-links">
                <RiFacebookFill className="sm-icon mx-3" />
                <RiTwitterFill className="sm-icon mx-3" />
                <RiInstagramLine className="sm-icon mx-3" />
                <RiLinkedinFill className="sm-icon mx-3" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
