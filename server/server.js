// const creds = require('./nodemon.json');

// console.log(creds)

let express = require("express");
let app = express();
let nodemailer = require("nodemailer");
const bodyParser = require("body-parser");
const cors = require("cors");

const path = require("path");
const router = express.Router();

// Static folder
app.use("/public", express.static(path.join(__dirname, "public")));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

let transporter = nodemailer.createTransport({
  // host: "smtp-mail.outlook.com",
  host: "smtp.gmail.com",
  service: "gmail",
  port: 587,
  secure: false,
  // service: "Gmail",
  // auth: {
  //   user: creds.auth.user,
  //   pass: creds.auth.pass
  // },
  // OUTLOOK
  // auth: {
  //   user: "cureeasy.test@outlook.com",
  //   pass: "cureeasy@555",
  // },
  // GMAIL
  auth: {
    user: "infoziumgallery@gmail.com",
    pass: "Infozium@123",
  },
  // tls: {
  //   ciphers: "SSLv3",
  // },
});

app.post("/mail", (req, res, next) => {
  try {
    var email = req.body.email;
    var name = req.body.name;
    var date = req.body.date;
    var contact = req.body.contact;
    var specilization = req.body.specilization;

    console.log(email);

    const mailOptions = {
      from: email,
      to: "infoziumgallery@gmail.com",
      subject: `*Appointment*`,
      html: `<label>Name: <label>${name} <br /> <label>Date: <label>${date} <br /> <label>Email: <label>${email} <br /> <label>Contact: <label>${contact} <br /> <label>Specilization: </label>${specilization}`,
    };

    transporter.sendMail(mailOptions, (err, data) => {
      if (err) {
        res.json({
          status: "err",
        });
        console.log(err);
      } else {
        res.json({
          status: "success",
        });
        console.log("Email Sent " + data.response);
      }
    });
  } catch (error) {
    console.log("Error Main:", error);
  }
});

transporter.verify(function (error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log("Server is ready to take our messages!");
  }
});

// port
const port = process.env.PORT || 8001;
app.listen(port, () => console.log(`Sever is running on ${port}!`));
